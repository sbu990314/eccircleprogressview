//
//  CircleView.h
//  DrawArc
//
//  Created by PO-YUSU on 2015/11/4.
//  Copyright © 2015年 SBU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ECCircleProgressView : UIView

/**
 * 線的顏色 (預設為藍色)
 */
@property (weak, nonatomic) UIColor *lineColor;

/**
 * 線條寬度 (預設為 6)
 */
@property (nonatomic) CGFloat lineWidth;

/**
 * 進度 (介於 0~1 之間)
 */
@property (nonatomic) CGFloat progress;

@end
