//
//  CircleView.m
//  DrawArc
//
//  Created by PO-YUSU on 2015/11/4.
//  Copyright © 2015年 SBU. All rights reserved.
//

#import "ECCircleProgressView.h"

#define   DEGREES_TO_RADIANS(degrees)  ((M_PI * degrees)/ 180)

@interface ECCircleProgressView ()
@property (strong ,nonatomic) UIBezierPath *path;
@property (strong ,nonatomic) CAShapeLayer *arcLayer;
@end

@implementation ECCircleProgressView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.path = [UIBezierPath bezierPath];
    
    self.arcLayer = [CAShapeLayer layer];
    
    //線尾圓的
    self.arcLayer.lineCap = @"round";
    
    //填滿顏色
    self.arcLayer.fillColor = [UIColor clearColor].CGColor;
    
    self.arcLayer.strokeColor = [UIColor blueColor].CGColor;
    
    self.arcLayer.lineWidth = 6;
    
    self.arcLayer.frame = self.bounds;
    
    [self.layer addSublayer:self.arcLayer];
}

- (UIColor *)lineColor
{
    return [UIColor colorWithCGColor:self.arcLayer.strokeColor];
}

- (void)setLineColor:(UIColor *)lineColor
{
    self.arcLayer.strokeColor = lineColor.CGColor;
}

- (CGFloat)lineWidth
{
    return self.arcLayer.lineWidth;
}

- (void)setLineWidth:(CGFloat)lineWidth
{
    self.arcLayer.lineWidth = lineWidth;
}

- (void)setProgress:(CGFloat)progress
{
    progress = MIN(1, progress);
    progress = MAX(0, progress);
    
    self->_progress = progress;
    
    [self draw];
}

- (void)draw
{
    CGFloat startAngle = -M_PI_2;
    CGFloat endAngle = -M_PI_2 + ((360 * self.progress) * M_PI / 180);
    
    [self.path removeAllPoints];
    [self.path addArcWithCenter:CGPointMake(CGRectGetWidth(self.bounds) * 0.5f, CGRectGetHeight(self.bounds) * 0.5f)
                         radius:(CGRectGetWidth(self.bounds) - self.arcLayer.lineWidth) * 0.5f
                     startAngle:startAngle
                       endAngle:endAngle
                      clockwise:YES];
    
    self.arcLayer.path = nil;
    self.arcLayer.path = self.path.CGPath;
}

@end
