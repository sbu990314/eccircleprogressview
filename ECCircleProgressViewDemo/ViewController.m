//
//  ViewController.m
//  DrawArc
//
//  Created by PO-YUSU on 2015/11/4.
//  Copyright © 2015年 SBU. All rights reserved.
//

#import "ViewController.h"

#import "MMTweenAnimation.h"
#import "PRTween.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.circleView.lineColor = [UIColor orangeColor];
    self.circleView.lineWidth = 12;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (IBAction)changeValueSlider:(UISlider *)sender
{
    CGFloat progress = sender.value;
    
    [PRTween tween:self.circleView
          property:@"progress"
              from:self.circleView.progress
                to:progress
          duration:0.6f
    timingFunction:PRTweenTimingFunctionCircOut
       updateBlock:^(PRTweenPeriod *period)
     {
         self.angleLabel.text = [NSString stringWithFormat:@"%ld", (NSInteger)(360 * period.tweenedValue)];
     }
     completeBlock:nil];
}
@end
