//
//  ViewController.h
//  DrawArc
//
//  Created by PO-YUSU on 2015/11/4.
//  Copyright © 2015年 SBU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECCircleProgressView.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet ECCircleProgressView *circleView;
@property (weak, nonatomic) IBOutlet UILabel *angleLabel;

@end

