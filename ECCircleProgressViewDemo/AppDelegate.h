//
//  AppDelegate.h
//  ECCircleProgressViewDemo
//
//  Created by PO-YUSU on 2015/11/19.
//  Copyright © 2015年 SBU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

